##  Daily Summary 

**Time: 2023/7/28**

**Author: 赖世龙**

---

**O (Objective):**   Today, the teacher introduced the topic and general direction of next week's project to us, and then our group decided what kind of project we should do after discussion, and then shared our ideas with the teacher in the elevator speech. In the afternoon, I also drew the user journey and user story of the project, which deepened the knowledge of the previous session. The teachers also showed us how to CI/CD and write a story card. I feel very fulfilled and accomplished today, which can only be attributed to the teamwork of the group.

**R (Reflective):**  Satisfied

**I (Interpretive):**   We strengthen feelings and cohesion in group cooperation, while stimulating our ideas and constructs 

**D (Decisional):**   As long as we stick together, our team will be unstoppable.

